/// //////////////////////////////////////////////////////////

const express = require('express') // app server
const request = require('request') // request module to make http requests
const bodyParser = require('body-parser') // parser for post requests
const Conversation = require('watson-developer-cloud/conversation/v1') // watson sdk, using only conversation service

/// //////////////////////////////////////////////////////////

const app = express()

const VariaveisAmbiente = {
  host: process.env.VCAP_APP_HOST || 'localhost', // vcap for bluemix/cloud
  port: process.env.VCAP_APP_PORT || 3000, // vcap port for bluemix/cloud
  token: process.env.TOKEN_FACEBOOK,
  watsonUrl: process.env.WATSON_URL_API,
  username: process.env.CONVERSATION_USERNAME,
  password: process.env.CONVERSATION_PASSWORD,
  workspace: process.env.WORKSPACE_ID,
  faceboookUrl: process.env.FACEBOOK_URL_API
}

const conversation = new Conversation({
  url: VariaveisAmbiente.watsonUrl,
  username: VariaveisAmbiente.username,
  password: VariaveisAmbiente.password,
  version: 'v1',
  version_date: '2017-05-26'
})

/// //////////////////////////////////////////////////////////

class WatsonAPI {
  constructor() {
    this.contexto = ''
  }
  construirPayload(mensagem) {
    let payload = {
      workspace_id: VariaveisAmbiente.workspace,
      input: {
        text: mensagem.replace('\n', '')
      }
    }
    if (this.contexto) {
      payload['context'] = this.contexto
    }
    return payload
  }
  comunicacao(mensagem, origemID) {
    conversation.message(this.construirPayload(mensagem), (err, responseMessage) => {
      this.contexto = responseMessage['context']
      if (err) return new Error(err)
      for (let indice = 0; indice < responseMessage.output.text.length; indice++) {
        FacebookAPI.enviarMesagem(origemID, responseMessage.output.text[indice])
      }
    })
  }
}
/// //////////////////////////////////////////////////////////

class FacebookAPI {
  static receberEventosDeRequest(req) {
    return req.body.entry[0].messaging // padrão de comunicação do facebook
  }
  static receberOrigemIdentificacao(evento) {
    return evento.sender.id // padrao de comunicao do facebook
  }
  static extrairTextoDeEventoMensagem(evento, texto) {
    if (evento.message && evento.message.text) {
      return evento.message.text
    } else if (evento.postback && texto === null) {
      return evento.postback.payload
    } else {
      return undefined
    }
  }
  static enviarMesagem(origemID, mensagem) {
    request({
      url: VariaveisAmbiente.faceboookUrl,
      method: 'POST',
      json: {
        recipient: {
          id: origemID
        },
        message: {
          text: mensagem
        }
      }
    }, function (error, response, body) {
      if (error) {
        console.error('Error não foi possivel enviar mensagem: ', error)
      } else if (response.body.error) {
        console.error('Error: ', response.body.error)
      }
    })
  }
}
/// //////////////////////////////////////////////////////////

class Servico {
  constructor() {
    this.facebookAPI = new FacebookAPI()
    this.watsonAPI = new WatsonAPI()
  }
  validarComunicaoFacebook(req, response) {
    if (req.query['hub.verify_token'] === VariaveisAmbiente.token) {
      response.send(req.query['hub.challenge'])
    } else {
      response.send('Erro ao tentar validar o token do facebook')
    }
  }
  comunicaoFacebook(req, response) {
    const self = servico
    const eventos = FacebookAPI.receberEventosDeRequest(req)
    let mensagem = null
    try {
      for (let indice = 0; indice < eventos.length; indice++) {
        const evento = eventos[indice]
        mensagem = FacebookAPI.extrairTextoDeEventoMensagem(evento, mensagem)
        if (mensagem) self.watsonAPI.comunicacao(mensagem, FacebookAPI.receberOrigemIdentificacao(evento))
      }
    } catch (error) {
      console.error(error)
    }
    response.sendStatus(200)
  }
}
/// //////////////////////////////////////////////////////////
const servico = new Servico()
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())
app.get('/webhook', servico.validarComunicaoFacebook)
app.post('/webhook', servico.comunicaoFacebook)
app.listen(VariaveisAmbiente.port, VariaveisAmbiente.host, function () {
  console.log('Server running on port: %d', VariaveisAmbiente.port)
})
